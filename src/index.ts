/** 
 * @project @beblurt/blurt-nodes-checker
 * @file Blurt Nodes Checker
 * @description Testing the different RPC nodes of the Blurt blockchain
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @license
 * Copyright (C) 2022  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** RxJS */
import { Subject } from "rxjs";

/** Test list to proceed */
import { testList } from "./test.js";

/** TYPE */
import { Client } from '@beblurt/dblurt'

import {
  OPTIONS, TEST_RPC_NODE, RPC_NODE, FULL_TEST
} from "./helpers/index.js"

export {
  TEST_RPC_NODE, RPC_NODE
} from "./helpers/index.js"

const RegExpUrl = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,=.]+$/

export class BlurtNodesChecker {
  nodes: RPC_NODE[] = []
  message = new Subject<RPC_NODE[]>()
  timer!: ReturnType<typeof setTimeout>

  debug       = false
  full        = false
  nexus       = true
  interval    = 900000
  timeout     = 3000
  reset?: number

  tests: FULL_TEST[] = testList(20000000, "beblurt", "communities-have-arrived-on-the-blurt-blockchain-and-beblurt-app-1685887788620")

  constructor(nodes: string[], options?:OPTIONS) {
    if(options) {
      if(options.debug && (typeof options.debug) === 'boolean') this.debug = options.debug
      if(options.full && (typeof options.full) === 'boolean') this.full = options.full
      if(options.nexus && (typeof options.nexus) === 'boolean') this.nexus = options.nexus
      if(options.interval && typeof options.interval === 'number') this.interval = options.interval * 1000
      if(options.timeout && typeof options.timeout === 'number') this.timeout = options.timeout * 1000
      if(options.reset && typeof options.reset === 'number') this.reset = options.reset
    }

    for (const node of nodes) {
      const isUrlValid = new RegExp(RegExpUrl)
      if(isUrlValid.test(node)) {
        this.nodes.push({
          url:         node,
          nb_ok:       0,
          nb_error:    0,
          nb_degraded: 0,
          last_check:  Date.now(),
          status:      "unkown",
          test_result: []
        })
      } else {
        this.message.error(new Error(`${node} is not a valid url`))
      }
    }
  }

  public async start(): Promise<void> {
    try {
      if(this.nodes.length > 0) {
        setTimeout(() => {
          this.check()
        }, 25);
        this.timer = setInterval(async () => {
          this.check()
        }, this.interval)
      }
    } catch (error) {
      clearInterval(this.timer)
      this.message.error((error as ReturnType<typeof Error>).message)
    }
  }

  public stop(): void {
    clearInterval(this.timer)
  }

  public restart(): void {
    clearInterval(this.timer)
    this.start()
  }

  private async nexusLight(node: RPC_NODE): Promise<boolean> {
    return new Promise( (resolve) => {
      const client = new Client(node.url, { timeout: this.timeout })
      client.nexus.accountNotifications("blurtofficial").then(notifs => {
        if(notifs) {
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(e => {
        if(this.debug) { console.warn('[BNC] Nexus Light: ', e instanceof Error ? e.message : 'Unknown error!') }
        resolve(false)
      })
    })
  }

  private check(): void {
    /** Define the execution time (used for sendMessage) */
    const checkTime = Date.now()

    /** Node Loop */
    for (const [i, node] of this.nodes.entries()) {
      /** We reach the reset number of tests => reinit counters value */
      if(this.reset && (node.nb_ok + node.nb_error + node.nb_degraded) === this.reset) {
        this.nodes[i]!.nb_ok = 0
        this.nodes[i]!.nb_error = 0
        this.nodes[i]!.nb_degraded = 0
        if(this.debug) { console.log('[BNC] reset counter', node.url) }
      }

      /** hrtime */
      const startRequestNode = Date.now()

      /** First request to BLURT node (light test) */
      const client = new Client(node.url, { timeout: this.timeout })
      client.condenser.getConfig().then(async config =>{
        /** hrtime */
        const milli = Date.now() - startRequestNode

        this.nodes[i]!.nb_ok++
        this.nodes[i]!.status   = "online"
        this.nodes[i]!.version  = config.BLURT_BLOCKCHAIN_VERSION 
        this.nodes[i]!.duration = milli
        if(this.debug) { console.log('[BNC]', node.url, 'updated') }

        /** try Nexus if asked (light test) */
        if (this.nexus) {
          node.nexus = await this.nexusLight(node)
          this.nodes[i]!.nexus = node.nexus
          if(this.debug) { console.log('[BNC]', node.url, 'Nexus =>', node.nexus) }
        }

        /** Execute a full test */
        if(this.full) {
          /** Define the number of test to proceed */
          const doNexus   = this.nexus && node.nexus ? true : false
          const doNbTests = doNexus ? this.tests.length : this.tests.filter(t => !t.nexus).length
          if(this.debug) { console.log('[BNC]', node.url, 'Nb Tests to proceed =', doNbTests, 'Proceed Nexus =>', doNexus) }

          /** Test Loop */
          for (const test of this.tests) {
            if(doNexus || !test.nexus) {
              /** hrtime */
              const startRequestTest = Date.now()

              /** Check if previous test */
              const tIndex  = node.test_result.findIndex(t => t.method === test.method)

              /** Execute the test */
              const method = test.method.split('.')
              if(method[0] && method[1]) {
                client.call(method[0], method[1], test.params).then(result => {
                  /** hrtime */
                  const milli = Date.now() - startRequestTest
  
                  const success = result && test.validator ? test.validator(result) : false
                  
                  /** Result of the test */
                  const r:TEST_RPC_NODE = {
                    name:        test.name,
                    description: test.description,
                    method:      test.method,
                    success:     success,
                    duration:    milli,
                    last_check:  checkTime
                  }

                  /** If test validation failed */
                  if(!success) {
                    r.error = "validation failed!"
                    /** update status of node */
                    this.nodes[i]!.status = "degraded"
                    /** update degraded counter */
                    this.nodes[i]!.nb_degraded++
                    /** correct OK counter */
                    if(this.nodes[i]!.nb_ok  > 0) this.nodes[i]!.nb_ok--
                    /** Update average time */
                    delete this.nodes[i]!.average_time
                  } else {
                    /** update status of node */
                    if(this.nodes[i]!.status !== "degraded") {
                      this.nodes[i]!.status = "online"
                    }
                    /** Update average time */
                    this.nodes[i]!.average_time = this.nodes[i]!.average_time ? Math.round(((this.nodes[i]!.average_time as number) + r.duration)/ 2) : r.duration
                  }

                  /** If old result update else add */
                  if(tIndex >= 0) {
                    this.nodes[i]!.test_result[tIndex] = r
                  } else {
                    this.nodes[i]!.test_result.push(r)
                  }

                  /** End of tests for the node? */
                  const nbTests = this.nodes[i]!.test_result.filter(n => n.last_check === checkTime).length
                  if(this.debug && nbTests === doNbTests) { console.log('[BNC]', node.url, 'Proceed test =>', nbTests, '/', doNbTests) }
                  if(nbTests === doNbTests) {
                    /** assign the execution time */
                    this.nodes[i]!.last_check = checkTime
  
                    /** Send message? */
                    this.sendMessage(checkTime)
                  }
                }).catch(error => {
                  /** Result of the test */
                  const r:TEST_RPC_NODE = {
                    name:        test.name,
                    description: test.description,
                    method:      test.method,
                    success:     false,
                    duration:    999999,
                    last_check:  checkTime,
                    error:       error.message
                  }
  
                  /** update status of node */
                  this.nodes[i]!.status = "degraded"
                  /** update degraded counter */
                  this.nodes[i]!.nb_degraded++
                  /** correct OK counter */
                  this.nodes[i]!.nb_ok--
  
                  /** If old result update else add */
                  if(tIndex >= 0) {
                    this.nodes[i]!.test_result[tIndex] = r
                  } else {
                    this.nodes[i]!.test_result.push(r)
                  }
  
                  /** Update average time */
                  delete this.nodes[i]!.average_time
  
                  /** End of tests for the node? */
                  const nbTests = this.nodes[i]!.test_result.filter(n => n.last_check === checkTime).length
                  if(this.debug) { console.log('[BNC]', node.url, 'ERROR Proceed test =>', nbTests, '/', doNbTests) }
                  if(nbTests === doNbTests) {
                    /** assign the execution time */
                    this.nodes[i]!.last_check = checkTime
  
                    /** Send message? */
                    this.sendMessage(checkTime)
                  }
                })
              }
            }
          }
        } else {
          /** assign the execution time */
          this.nodes[i]!.last_check = checkTime

          /** Send message? */
          this.sendMessage(checkTime)
        }
      }).catch(error => {
        this.nodes[i]!.nb_error++
        this.nodes[i]!.status = "error"
        if(error) this.nodes[i]!.error  = (error as ReturnType<typeof Error>).message

        /** assign the execution time */
        this.nodes[i]!.last_check = checkTime

        /** Send message? */
        this.sendMessage(checkTime)
      })
    }
  }            

  private async sendMessage(checkTime: number): Promise<void> {
    /** Send message? */
    const nbNodeChecked = this.nodes.filter(n => n.last_check === checkTime)
    if(this.debug) { console.log('[BNC]', `nbNodeChecked = ${nbNodeChecked.length}`) }

    if(this.nodes.length === nbNodeChecked.length) this.message.next(this.nodes)
  }
}
