/** 
 * @project @beblurt/blurt-nodes-checker
 * @file Full tests
 * @description Testing the different RPC nodes of the Blurt blockchain
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @license
 * Copyright (C) 2022  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** TYPE */
import { FULL_TEST } from "./helpers/index.js"
import {
  AppliedOperation,
  BlogEntry,
  Discussion,
  DynamicGlobalProperties,
  ExtendedAccount,
  NexusAccountNotifications,
  NexusPayoutStats,
  NexusPost,
  NexusProfile,
  NexusCommunity,
  Post,
  SignedBlock
} from "@beblurt/dblurt"

export const testList = (block: number, account: string, permlink: string): FULL_TEST[] => {
  return [
    {
      name: "Nexus | account_notifications ",
      description: "Get account notifications",
      nexus: true,
      method: "bridge.account_notifications",
      params: { account, min_score: 25, last_id: null, limit: 10 },
      validator: (result) => Array.isArray(result as NexusAccountNotifications[]) && (result as NexusAccountNotifications[]).length > 0  ? true : false
    },
    {
      name: "Nexus | get_account_posts ",
      description: "Blog, feed, replies... lists of an account",
      nexus: true,
      method: "bridge.get_account_posts",
      params: { sort: 'feed', account, limit: 10 },
      validator: (result) => {
        return Array.isArray(result as NexusPost[]) && (result as NexusPost[]).length > 0  ? true : false
      }
    },
    {
      name: "Nexus | get_discussion",
      description: "Gives a flattened discussion tree",
      nexus: true,
      method: "bridge.get_discussion",
      params: { author: account, permlink },
      validator: (result) => {
        return Object.keys(result as {[key: string]: NexusPost}).length > 0  ? true : false
      }
    },
    {
      name: "Nexus | get_payout_stats ",
      description: "Get payout stats",
      nexus: true,
      method: "bridge.get_payout_stats",
      params: { limit: 5 },
      validator: (result) => {
        return (result as NexusPayoutStats).items ? true : false
      }
    },
    {
      name: "Nexus | get_profile ",
      description: "Resume of a profile",
      nexus: true,
      method: "bridge.get_profile",
      params: { account, observer: null },
      validator: (result) => {
        return (result as NexusProfile).created ? true : false
      }
    },
    {
      name: "Nexus | get_ranked_posts ",
      description: "Get ranked posts (Hot, Trending, Created...)",
      nexus: true,
      method: "bridge.get_ranked_posts",
      params: { sort: 'created', limit: 10, tag: 'blurt-101010', observer: account },
      validator: (result) => {
        return Array.isArray(result as NexusPost[]) && (result as NexusPost[]).length > 0  ? true : false
      }
    },
    {
      name: "Nexus | list_communities ",
      description: "List of communities",
      nexus: true,
      method: "bridge.list_communities",
      params: { last: null, limit: 10, query: null, sort: 'rank', observer: account },
      validator: (result) => Array.isArray(result as NexusCommunity[]) && (result as NexusCommunity[]).length > 0  ? true : false
    },
    {
      name: "Condenser | Get Account History",
      description: "History of operations for a given account",
      nexus: false,
      method: "condenser_api.get_account_history",
      params: [account, -1, 10],
      validator: (result) => Array.isArray(result as [number, AppliedOperation]) && (result as [number, AppliedOperation]).length > 0  ? true : false
    },
    {
      name: "Condenser | Get Accounts",
      description: "Retrieve an account details",
      nexus: false,
      method: "condenser_api.get_accounts",
      params: [[account]],
      validator: (result) => Array.isArray(result as ExtendedAccount[]) && (result as ExtendedAccount[]).length > 0  ? true : false
    },
    {
      name: "Condenser | Get Block",
      description: "Get Block",
      nexus: false,
      method: "condenser_api.get_block",
      params: [block],
      validator: (result) => {
        return (result as SignedBlock).witness ? true : false
      }
    },
    {
      name: "Condenser | Get Blog Entries",
      description: "Retrieve the list of blog entries for an account",
      nexus: false,
      method: "condenser_api.get_blog_entries",
      params: [account, 0, 25],
      validator: (result) => Array.isArray(result as BlogEntry[]) && (result as BlogEntry[]).length > 0  ? true : false
    },
    {
      name: "Condenser | Get Content",
      description: "Retrieve the content (post or comment)",
      nexus: false,
      method: "condenser_api.get_content",
      params: [account, permlink],
      validator: (result) => {
        return (result as Post).created ? true : false
      }
    },
    {
      name: "Condenser | Get Dynamic Global Propertie",
      description: "Check chain global properties",
      nexus: false,
      method: "condenser_api.get_dynamic_global_properties",
      params: [],
      validator: (result) => {
        return "head_block_number" in (result as DynamicGlobalProperties) && "last_irreversible_block_num" in (result as DynamicGlobalProperties) ? true : false
      }
    },
    {
      name: "Condenser | Get Disccusion By Blog",
      description: "Retrieve a list of discussions by blog",
      nexus: false,
      method: "condenser_api.get_discussions_by_blog",
      params: [{ tag: account, limit: 5, truncate_body: 1}],
      validator: (result) => Array.isArray(result as Discussion[]) && (result as Discussion[]).length > 0  ? true : false
    },
    {
      name: "Condenser | Lookup Account",
      description: "Looks up accounts starting with name",
      nexus: false,
      method: "condenser_api.lookup_accounts",
      params: ["nale",10],
      validator: (result) => Array.isArray(result as string[]) && (result as string[]).length > 0  ? true : false
    },
  ]
}