/** 
 * @project @beblurt/blurt-nodes-checker
 * @file Type definition
 * @author BeBlurt <https://beblurt.com/@beblurt>
 * @license
 * Copyright (C) 2022  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** TYPE */
import {
  AppliedOperation,
  BlogEntry,
  Discussion,
  DynamicGlobalProperties,
  ExtendedAccount,
  NexusAccountNotifications,
  NexusPayoutStats,
  NexusPost,
  NexusProfile,
  NexusCommunity,
  Post,
  RpcNodeConfig,
  SignedBlock
} from "@beblurt/dblurt"

export declare type OPTIONS = {
  debug?:    boolean
  full?:     boolean
  nexus?:    boolean
  timeout?:  number
  interval?: number
  reset?:    number
}

export declare type TEST_RPC_NODE = {
  name:        string
  description: string
  method:      string
  success:     boolean
  duration:    number
  error?:      string
  last_check:  number
}

export declare type RPC_NODE = {
  url:            string
  nb_ok:          number
  nb_error:       number
  nb_degraded:    number
  error?:         string
  last_check:     number
  status:         "unkown"|"online"|"degraded"|"error"
  duration?:      number
  average_time?:  number
  version?:       string
  deactivated?:   boolean

  test_result:   Array<TEST_RPC_NODE>

  nexus?: boolean
}

type AccountNames = string[]

export declare type FULL_TEST = {
  name:        string
  description: string
  nexus:       boolean
  method: 
  | "bridge.account_notifications"
  | "bridge.get_account_posts"
  | "bridge.get_discussion"
  | "bridge.get_payout_stats"
  | "bridge.get_profile"
  | "bridge.get_ranked_posts"
  | "bridge.list_communities"
  | "condenser_api.get_account_history"
  | "condenser_api.get_accounts"
  | "condenser_api.get_block"
  | "condenser_api.get_blog_entries"
  | "condenser_api.get_content"
  | "condenser_api.get_dynamic_global_properties"
  | "condenser_api.get_discussions_by_blog"
  | "condenser_api.lookup_accounts"
  params: unknown
  validator: (
    result: 
    | RpcNodeConfig
    | Array<NexusAccountNotifications>
    | NexusPayoutStats
    | NexusPost
    | NexusPost[]
    | {[key: string]: NexusPost}
    | NexusProfile
    | Array<NexusCommunity>
    | [number, AppliedOperation]
    | Array<ExtendedAccount>
    | SignedBlock
    | Array<BlogEntry>
    | Post
    | DynamicGlobalProperties
    | Array<Discussion>
    | AccountNames
  ) => boolean
}
