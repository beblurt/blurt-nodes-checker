# Blurt RPC nodes checker

ESM Typscript [Blurt blockchain](https://gitlab.com/blurt/blurt) RPC nodes servers checker (latency, availability, methods).

Checks the status of the RPC nodes servers of the Blurt blockchain at a specified interval and sends a report via a RxJS subscription. The result is stored in an array in memory and you can call it at any time.

The library also allows you to identify the compatibility of an RPC node with [Nexus](https://gitlab.com/blurt/openblurt/nexus) (communities on the [Blurt blockchain](https://gitlab.com/blurt/blurt)) and to adapt the full test accordingly by including the Nexus methods.

## Getting Started

### Installation

#### Using npm:

```bash
$  npm install @beblurt/blurt-nodes-checker --save
```

#### Using browser:

```html
<script type="module" src="./dist/blurt-nodes-checker.min.js"></script>
```

#### From the source:

clone the repository

```bash
$  git clone https://gitlab.com/beblurt/blurt-nodes-checker.git
```

Build for NodeJS/Angular... in `"./lib"` directory

```bash
$  npm run build
```

Build for browser (minified) in `"./dist"` directory

```bash
$  npm run build-browser
```

### Initialisation

Initialise the Blurt nodes checker with **params**

```js
/** Init the checker */
const nodesChecker = new BlurtNodesChecker(["url1", "url2", ...])
```

### Params
- `url`: array of node url to check
- `options` (optional):
    - `debug` (boolean): if true show console info (default: false) 
    - `full` (boolean): if false perform only a `get_config` with response time (default: false)
    - `nexus` (boolean): if true check if the rpc node is [Nexus](https://gitlab.com/blurt/openblurt/nexus) compatible (default: true)
    - `interval` (seconds): delay in seconds between each execution (default: 900 seconds)
    - `timeout` (seconds): timeout in seconds for node request (default: 3 seconds)
    - `reset` (number): number max of test before resetting the counters (default: no reset)

### Functions

#### start()

Start the checking process

```js
/** Start the checker */
nodesChecker.start()
```

#### restart()

restart the checking process with the same **params**

```js
/** Restart the checker */
nodesChecker.restart()
```

#### stop()

stop the checking process

```js
/** Stop the checker */
nodesChecker.stop()
```

### Message subscription

#### message.subscribe()

It use RxJS to send the result of a check

```js
/** Subscribe results */
nodesChecker.message.subscribe({
  next: async m => {
    ...
  },
  error: error => {
    ...
  }
})
```

#### message.unsubscribe()

Unsubscribe manually from the Observable

```js
/** Unsubscribe results */
nodesChecker.message.unsubscribe()
})
```

### Example usage

Performing a full checking:

```js
import { BlurtNodesChecker } from '@beblurt/blurt-nodes-checker'

/** Blurt nodes url to check */
const nodes = [
  "https://rpc.beblurt.com",
  "https://blurt-rpc.saboin.com",
  "https://rpc.blurt.world",
  "https://blurtrpc.actifit.io",
  "https://kentzz.blurt.world",
  "https://rpc.blurt.live",
  "https://blurtdev.techcoderx.com"
]

/** Options */
const options = {
  full: true,
  nexus: true,
  interval: 600, // 10 minutes
  reset: 144 // every 144 tests => 24 hours x 6 (10 minutes = 6 test per hours)
}
/** Init the checker */
const nodesChecker = new BlurtNodesChecker(nodes, options)
/** Start the checker */
nodesChecker.start()
/** Subscribe results */
nodesChecker.message.subscribe({
  next: async m => {
    console.log('=====> NEW MESSAGE', m)
    console.log()
  },
  error: error => {
    console.log('=====> ERROR MESSAGE')
    console.log(error)
    console.log()

    /** Restart the checker */
    nodesChecker.restart()
  }
})  
```

### Light checking

In this case, only a call to the rpc method `condenser_api.get_config` is made, if `nexus: true` in options there is a call to the rpc method `bridge.unread_notifications` too to check if it's a Nexus rcp node.

#### Result
```ts
[{
  "url":            string
  "nb_ok":          number
  "nb_error":       number
  "nb_degraded":    number
  "error"?:         string
  "last_check":     number
  "status":         "unkown"|"online"|"degraded"|"error"
  "duration"?:      number
  "average_time"?:  number
  "version"?:       string
  "deactivated"?:   boolean
  "nexus":          boolean

  "test_result":    []
}]
```

#### Exemple of result for a light checking
```js
[
  {
    url: "https://rpc.beblurt.com",
    nb_ok: 1,
    nb_error: 0,
    nb_degraded: 0,
    last_check: 1669607084463,
    status: "online",
    test_result: [],
    version: "0.8.2",
    duration: 598,
    nexus: true
  },
  ...
]
```

### Full checking

In this case the methods below are checked:

- `condenser_api.get_config`
- `condenser_api.get_account_history`
- `condenser_api.get_accounts`
- `condenser_api.get_block`
- `condenser_api.get_blog_entries`
- `condenser_api.get_content`
- `condenser_api.get_dynamic_global_properties`
- `condenser_api.get_discussions_by_blog`
- `condenser_api.lookup_accounts`

If it is a Nexus node the following methods are also checked:

- `bridge.account_notifications`
- `bridge.get_account_posts`
- `bridge.get_discussion`
- `bridge.get_payout_stats`
- `bridge.get_profile`
- `bridge.get_ranked_posts`
- `bridge.list_communities`

#### Result
```ts
[{
  "url":            string
  "nb_ok":          number
  "nb_error":       number
  "nb_degraded":    number
  "error"?:         string
  "last_check":     number
  "status":         "unkown"|"online"|"degraded"|"error"
  "duration"?:      number
  "average_time"?:  number
  "version"?:       string
  "deactivated"?:   boolean

  "test_result":   [{
    "name":        string
    "description": string
    "method":      string
    "success":     boolean
    "duration":    number
    "error"?:      string
    "last_check":  number
  }]
}]
```

#### Exemple of result for a full checking
```js
[
  {
    url: "https://rpc.beblurt.com",
    nb_ok: 1,
    nb_error: 0,
    nb_degraded: 0,
    last_check: 1669607748639,
    status: "online",
    test_result: [
        {
            name: "Bridge | account_notifications ",
            description: "Account notifications",
            method: "bridge.account_notifications",
            success: true,
            duration: 314,
            last_check: 1669607748639
        },
        ...
        {
            name: "Condenser | Get Disccusion By Blog",
            description: "Retrieve a list of discussions by blog",
            method: "condenser_api.get_discussions_by_blog",
            success: true,
            duration: 1181,
            last_check: 1669607748639
        }
    ],
    version: "0.8.2",
    duration: 579,
    nexus: true,
    average_time: 1140,
  },
  ...
]
```

## Contributing

Pull requests for new features, bug fixes, and suggestions are welcome!

## Author

@beblurt (https://beblurt.com/@beblurt)

## License

 Copyright (C) 2022  IMT ASE Co., LTD
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.