# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.2](https://gitlab.com/beblurt/blurt-nodes-checker/compare/v1.1.1...v1.1.2) (2023-08-14)


### Code Refactoring

* **nexus:** add some Nexus and Condenser methods + modify default nexus option ([eb52f10](https://gitlab.com/beblurt/blurt-nodes-checker/commit/eb52f10911c4037b9239d907e6bd176272f12992))

### [1.1.1](https://gitlab.com/beblurt/blurt-nodes-checker/compare/v1.1.0...v1.1.1) (2022-11-29)


### Code Refactoring

* **axios:** remove axios to use only dblurt ([fe99b64](https://gitlab.com/beblurt/blurt-nodes-checker/commit/fe99b64c04037e55c3b204949fedc59e76ff9cd1))

## [1.1.0](https://gitlab.com/beblurt/blurt-nodes-checker/compare/v1.0.1...v1.1.0) (2022-11-28)


### Features

* **nexus:** add Nexus identification and tests ([ff92651](https://gitlab.com/beblurt/blurt-nodes-checker/commit/ff92651698c49233e685fc053313ef1bcd403919))

### 1.0.1 (2022-08-30)
