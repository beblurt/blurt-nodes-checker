import { build } from 'esbuild'

build({
  entryPoints: ['src/index.ts'],
  outfile: 'dist/blurt-nodes-checker.min.js',
  bundle: true,
  minify: true,
  // sourcemap: true,
  format: 'esm',
  platform: 'browser',
  target: ['esnext']
})